#This  file checks for existing software preference files and updates new previous settings

from os import path, makedirs

class Settings():

    def __init__(self):
        if path.exists("settings") != True: #make settings folder if doesn't exist
            makedirs("settings")
        else:
            pass

        self.setExists = path.exists("settings/preference.txt") #check file exists

    def savePref(self, data): #Save new preferences from gui
        if  self.setExists == True:
            self.f = open("settings/preference.txt", 'w')
            self.f.truncate(0)
            self.f.write(str(data))
            self.f.close()

    def updatePref(self): #Update preferences in gui
        if  self.setExists == True:
            try:
                self.f = open("settings/preference.txt", 'r')
                data = self.f.readlines(1)[0].strip('][').split(', ')
                self.f.close()
                return data
            except Exception:
                self.f.close()
        else:
            self.f = open("settings/preference.txt", 'w')
            self.f.close()


            








