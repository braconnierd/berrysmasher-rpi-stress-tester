#This file is the gui for setting initial parameters prior to running test

from tkinter import Tk, Label, Entry, Button, Text, END, WORD
from tkinter.filedialog import asksaveasfilename

#Custom modules
from data_collection import Record
from preferenceHandler import Settings

if __name__ == '__main__':

    class gui():

        def __init__(self):
            self.window = Tk()
            self.window.title("Berry Smasher - Load Test")
            self.window.configure(background = "black")
            self.Labels = ["Sample Name", 
                            "Sample Rate (sec)", 
                            "Idle Time (min)", 
                            "Stress Time (min)", 
                            "Cool Time (min)"]

            #Text Field
            T1 = Text(self.window, wrap = WORD, bg = 'black', fg = 'white', insertbackground = 'white', bd = 0, height = 4, width = 10)
            T1.grid(row = 2, column = 4, rowspan = 3, padx = (0,4) , pady =  4)

            #Form Labels
            for label in self.Labels:
                Label(self.window, text = str(label), background = 'black', foreground = 'white').grid(row = self.Labels.index(label), column = 0, padx = 4, pady = 4, sticky = "w,e")
                    
            #Form Entries        
            E1 = Entry(self.window, background = 'black', foreground = 'white', insertbackground = 'white')
            E1.grid(row = 0, column = 2, padx = 4, pady = 4)
            E2 = Entry(self.window, background = 'black', foreground = 'white', insertbackground = 'white')
            E2.grid(row = 1, column = 2, padx = 4, pady = 4)
            E3 = Entry(self.window, background = 'black', foreground = 'white', insertbackground = 'white')
            E3.grid(row = 2, column = 2, padx = 4, pady = 4)
            E4 = Entry(self.window, background = 'black', foreground = 'white', insertbackground = 'white')
            E4.grid(row = 3, column = 2, padx = 4, pady = 4)
            E5 = Entry(self.window, background = 'black', foreground = 'white', insertbackground = 'white')
            E5.grid(row = 4, column = 2, padx = 4, pady = 4)

            #ButtonCommand
            def Start():
                if E1.get() != '' and E2.get() != '' and E3.get() != '' and E4.get() != '' and E5.get() != '': #Check that fields are full
                    self.setData = [E1.get(), float(E2.get()), float(E3.get()), float(E4.get()), float(E5.get())] #Record preferences
                    Settings().savePref(self.setData) #Save new settings
                    
                    T1.config(fg = 'lime')
                    T1.insert(END, "Test has been started...")

                    Record().run(E1.get(), float(E2.get()), float(E3.get()), float(E4.get()), float(E5.get())) #Run test

            #Start button    
            Button(self.window, text = "Start Test", command = Start).grid(row = 0, column = 3, rowspan = 2, columnspan = 2, padx = (0,4) , pady = 4, sticky = "n,s,e,w")

            #Update preferences
            upData = Settings().updatePref()

            if isinstance(upData, list) == True:
                E1.insert(0, upData[0].strip('\''))
                E2.insert(0, upData[1])
                E3.insert(0, upData[2])
                E4.insert(0, upData[3])
                E5.insert(0, upData[4])
            else: 
                pass

            #Launch window
            self.window.resizable(False, False)
            self.window.mainloop()

    #Start gui
    gui()