#This defines the os system calls to gather CPU temp, load, and frequency

import os

class Measure():

    def measure_temp(self):
        return('temp')
    
    def measure_load(self):
        return('load')

    def measure_freq(self):
        return('freq')
