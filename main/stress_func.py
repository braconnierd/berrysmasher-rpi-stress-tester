#This is the main module to apply and control stress on a CPU

"""
Produces load on all available CPU cores
"""
from multiprocessing import Pool, cpu_count
from threading import Thread, Timer

class Stress():
    
    def __init__(self, StressTime, today):
        self.stop_loop = 0
        self.stress_time = StressTime
        self.L = open(str('logfiles/' + today + "_log.txt"),'a')

    def stop_stress(self):
        self.stop_loop = 1

    def loop(self, x):
        Timer(self.stress_time, self.stop_stress).start() #Stop stress after stress_time
        while self.stop_loop != 1:
            (x*x)^2 #testing new extremes (normally: x*x)
            
    def stress(self):
        processes = cpu_count()
        self.L.write('Running load on CPU(s) \n')
        self.L.write('Utilizing %d cores \n' % processes)
        self.L.write('Duration ' + str(float(self.stress_time/60)) + ' minutes... \n')
        self.L.close()
        pool = Pool(processes)
        pool.map_async(self.loop, range(processes)) #Run loop stress asynchronusly 