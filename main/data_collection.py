#This file will monitor and control stress function and recording processes

from stress_func import Stress
from monitor_temp import Measure
from multiprocessing import Process
from datetime import datetime
from polling import poll
from tkinter import Tk
from tkinter.filedialog import asksaveasfilename
from numpy import around, arange
from time import sleep
from sys import exit
from os import path, makedirs
class Record():

    def __init__(self):
        self.CPU_temp = 0 #Tempurature value of cpu (deg C)
        self.CPU_load = 0 #Percent cpu load (%)
        self.CPU_freq = 0 #Frequency value of cpu (GHz)
        self.SampleRate = 0 #Sample rate of data recording (seconds)
        self.TotalTime = 0 #Total time for test (seconds)
        self.Time = 0 #Time value since stress test initiation
        self.SampleName = "" #String of sample name for test 
        self.today = datetime.today().strftime("%m-%d-%Y") #Date for log file naming

        self.temp_array = [] #CPU temp array for quick calculation of max and min temps during test

        if path.exists("logfiles") != True: #make logfiles folder if doesn't exist
            makedirs("logfiles")
        else:
            pass

    def monitor(self):
        self.CPU_temp = Measure().measure_temp() #Update temp value
        self.CPU_load = Measure().measure_load() #Update load value
        self.CPU_freq = Measure().measure_freq() #Update frequency value

        self.temp_array.append(self.CPU_temp) #Append array of CPU temp values 

        data = str(round(self.Time, 2)) + '\t' + str(self.CPU_temp) + '\t' + str(self.CPU_load) + '\t' + str(self.CPU_freq) + '\n'
        self.f.write(data) #Record data every SampleRate interval (seconds)
        print(data) #Push data to screen

    def finalMetrics(self): #calculate min and max values of stress test fom temp array
        temp_min = min(self.temp_array)
        temp_max = max(self.temp_array)
        temp_avg = sum(self.temp_array) / len(self.temp_array) #gross average of whole test
        temp_output = str("Min Temp (deg C): " + temp_min + '\n' + "Max Temp (deg C): " + temp_max + '\n' + "Gross Avg Temp (deg C): " + temp_avg + '\n')
        print (temp_output)
        return (temp_output) 

    def finish(self):
        self.f.close() #Save data after stress test complete'
        self.L = open(str('logfiles/' + self.today + "_log.txt"),'a') #Open log file
        self.L.write(self.finalMetrics()) #Write summary metrics
        self.L.write('The Berry Has Been Smashed >_< \n')
        self.L.close()
        #exit() #Exit code after completion ?maybe?

    def run(self, SampleName, Rate, IdleTime, StressTime, CoolTime):

        self.SampleName = SampleName #Define sample name for data collection file
        self.SampleRate = Rate #Define sample rate for data collection
        self.TotalTime = float((IdleTime + StressTime + CoolTime)*60) #Define total time (seconds)

        try:
            Tk().withdraw()
            self.Data_file = str(asksaveasfilename(title = "Select -or- Create Text File", filetypes = [("Text File", "*.txt"), 
                                                    ("All Files", "*.*")], defaultextension = ("*.txt"))) #Save location for recorded information
            print(self.Data_file)
            self.f = open(self.Data_file, 'a') #Open file to record data

            #Create header for data file
            startTime = "Start time (y-m-d h:m:s.ms)" + '\t' + str(datetime.now()) + '\n'
            sampleName = "Sample Name" + '\t' + self.SampleName + '\n'
            tableHeaders = "Time (s)" + '\t' + "Temp (deg C)" + '\t' + "Load (%)" + '\t' + "Freq (GHz)" + '\n'
            self.f.write(startTime + sampleName + tableHeaders) #Write header information to data file
            print(tableHeaders) #Oush headers to screen

        except Exception: #Must select file to continue
            #print('Exit code: file not selected')
            exit() #Exit code: file not selected

        for self.Time in arange(0, self.TotalTime, self.SampleRate):
            self.monitor() 

            if self.Time == 0:
                self.L = open(str('logfiles/' + self.today + "_log.txt"),'a') #Open log file
                self.L.write(str('\nTest started: ' + str(datetime.now()) + ' \n'))
                self.L.write('Idle for ' + str(IdleTime) + ' minutes... \n')
                self.L.close()

            if self.Time == IdleTime*60:
                Stress(StressTime*60, self.today).stress()
            
            if self.Time == (IdleTime + StressTime)*60:
                self.L = open(str('logfiles/' + self.today + "_log.txt"),'a') #Open log file
                self.L.write('Cool down for ' + str(CoolTime) + ' minutes... \n')

            sleep(self.SampleRate)

            self.Time += self.SampleRate
        
        self.finish()