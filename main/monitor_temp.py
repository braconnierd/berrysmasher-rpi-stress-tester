#This defines the os system calls to gather CPU temp, load, and frequency

from psutil import cpu_percent, cpu_freq, sensors_temperatures

class Measure():

    def measure_temp(self):
        t1 = str(sensors_temperatures()['cpu-thermal'][0]).split(',')
        temp = t1[1].strip(" current=")
        return(temp)
    
    def measure_load(self):
        load = str(cpu_percent())
        return (load)

    def measure_freq(self):
        f1 = str(cpu_freq()).split(',')
        freq = str(float(f1[0].strip("scpufreq(current="))/1000)
        return (freq)